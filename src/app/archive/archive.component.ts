import { Component, OnInit } from '@angular/core';
import {TransactionData, TransactionDataService} from '../transaction-data-model/transaction-data-service';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.less']
})
export class ArchiveComponent implements OnInit {

  protected data: Array<TransactionData>;

  constructor(protected transactionDataService: TransactionDataService) {

  }

  updateSearchedItems(items: Array<TransactionData>) {
    this.data = items;

  }

  ngOnInit(): void {

    if (this.transactionDataService.httpRequestWasNotMade()) {
      this.transactionDataService.getHttpRequest()
        .subscribe((data: any[]) => {
          this.transactionDataService.insertDataToArray(data);
          this.data = this.transactionDataService.getShuffledArray();
          this.transactionDataService.httpRequestWasMade();
        });
    }
    this.data = this.transactionDataService.getShuffledArray();
  }

}

import {Component, Input, OnInit} from '@angular/core';
import {TransactionData, StatusType} from '../transaction-data-model/transaction-data-service';


@Component({
  selector: 'app-transaction-list-item',
  templateUrl: './transaction-list-item.component.html',
  styleUrls: ['./transaction-list-item.component.less']
})
export class TransactionListItemComponent implements OnInit {

  @Input() protected transactionData: TransactionData;
  protected statusTypeRef = StatusType;

  constructor() {
  }

  ngOnInit() {
  }


  protected formatDate() {
    return this.transactionData.date.format('DD MMM YYYY');
  }

}

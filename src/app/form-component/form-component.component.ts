import {Component, EventEmitter, Injector, OnInit, Output} from '@angular/core';
import {Accounts, StatusType, TransactionData, TransactionDataService} from '../transaction-data-model/transaction-data-service';
// import * as moment from 'moment';
import {Moment} from 'moment';
import {FormBuilder, FormGroup} from '@angular/forms';


export interface SearchFilters {
  statusType?: StatusType;
  accountType?: Accounts;
  message?: string;
  fromDate?: Moment;
  toDate?: Moment;
}

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.less']
})
export class FormComponentComponent implements OnInit {

  protected accountTypeItems: Array<string>;
  protected statusTypeItems: Array<string>;
  private fromDate: Moment;
  private toDate: Moment;
  searchForm: FormGroup;

  @Output() submitProcessed: EventEmitter<Array<TransactionData>>;

  constructor(private injector: Injector, fb: FormBuilder) {

    this.searchForm = fb.group({
      'message' : [],
      'accountType' : [],
      'statusType' : [],
      'fromDate':  [],
      'toDate' : []
    });

    this.submitProcessed = new EventEmitter();
    this.makeAccountTypeItems();
    this.makeStatusTypeItems();
  }

  ngOnInit() {

  }


  private makeAccountTypeItems() {

    this.accountTypeItems = [];

    for (const key in Accounts) {
      if (typeof Accounts[key] === 'string') {
        this.accountTypeItems.push(Accounts[key]);
      }
    }
  }

  private makeStatusTypeItems() {

    this.statusTypeItems = [];

    for (const key in StatusType) {
      if (typeof StatusType[key] === 'string') {
        this.statusTypeItems.push(StatusType[key]);
      }
    }
  }

  private validateDates() {
    if (this.fromDate && this.toDate) {
      if (this.fromDate > this.toDate) {
        alert('Ați ales data incorect');
      } else if (this.fromDate.isSame(this.toDate)) {
        alert('Ați ales aceeași dată');
      }
    }
  }

  dateFromBinding(value: Moment) {
    if (value) {
      this.fromDate = value;

    } else {
      this.fromDate = null;
    }

    this.validateDates();
  }

  dateToBinding(value: Moment) {
    if (value) {
      this.toDate = value;
    } else {
      this.toDate = null;
    }

    this.validateDates();
  }


  onSubmit(searchFilters: SearchFilters): void {

    if (searchFilters.accountType && searchFilters.accountType === Accounts.any) {
      searchFilters.accountType = null;
    }

    if (searchFilters.statusType && searchFilters.statusType === StatusType.any) {
      searchFilters.statusType = null;
    }


    const searchedItems: Array<TransactionData> =
      this.injector.get(TransactionDataService).startSearchByFilters(searchFilters);

    this.submitProcessed.emit(searchedItems);
  }

}

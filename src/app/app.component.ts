import {Component, OnInit} from '@angular/core';
import {TransactionData} from './transaction-data-model/transaction-data-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  protected data: Array<TransactionData>;

  constructor() {
  }

  ngOnInit() {
  }
}

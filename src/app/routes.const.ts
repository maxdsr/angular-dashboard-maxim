import {Routes} from '@angular/router';
import {AuthorizationComponent} from './authorization/authorization.component';
import {PendingComponent} from './pending/pending.component';
import {ErrorsComponent} from './errors/errors.component';
import {ArchiveComponent} from './archive/archive.component';

export const routes: Routes = [
  { path: '', redirectTo: 'archive', pathMatch: 'full' },
  { path: 'archive', component: ArchiveComponent},
  { path: 'auth', component: AuthorizationComponent},
  { path: 'pending', component: PendingComponent},
  { path: 'errors', component: ErrorsComponent}
];

import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { TabNavbarComponent } from './tab-navbar/tab-navbar.component';
import { TransactionListItemComponent } from './transaction-list-item/transaction-list-item.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import {NgxMultiLineEllipsisModule} from 'ngx-multi-line-ellipsis';
import { FormComponentComponent } from './form-component/form-component.component';
import {DpDatePickerModule} from 'ng2-date-picker';
import {TransactionDataService} from './transaction-data-model/transaction-data-service';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgSelectModule} from '@ng-select/ng-select';
import { ArchiveComponent } from './archive/archive.component';
import { AuthorizationComponent } from './authorization/authorization.component';
import { PendingComponent } from './pending/pending.component';
import { ErrorsComponent } from './errors/errors.component';
import {RouterModule} from '@angular/router';
import {routes} from './routes.const';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    TabNavbarComponent,
    TransactionListItemComponent,
    TransactionListComponent,
    FormComponentComponent,
    ArchiveComponent,
    AuthorizationComponent,
    PendingComponent,
    ErrorsComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxMultiLineEllipsisModule,
    NgSelectModule,
    DpDatePickerModule,
    RouterModule.forRoot(routes)
  ],
  providers: [TransactionDataService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }


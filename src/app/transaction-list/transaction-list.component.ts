import {Component, Input, OnInit} from '@angular/core';
import {TransactionData} from '../transaction-data-model/transaction-data-service';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.less']
})
export class TransactionListComponent implements OnInit {

  @Input() protected transactionDataArray: Array<TransactionData>;

  constructor() {
  }

  ngOnInit() {

  }

}

import {SearchFilters} from '../form-component/form-component.component';
import {Accounts, TransactionData, TransactionDataService} from './transaction-data-service';
import * as _ from 'lodash';
import * as moment from 'moment';
import {Moment} from 'moment';

export class SearchTransactionDataModel {

  private searchFilters: SearchFilters;
  private readonly dataArray: Array<TransactionData>;
  private readonly serviceInstance: TransactionDataService;

  constructor(searchFilters: SearchFilters, serviceInstance: TransactionDataService) {
    this.searchFilters = searchFilters;
    this.serviceInstance = serviceInstance;
    this.dataArray = this.serviceInstance.getShuffledArray();
  }


  private verifyByDate(arrayItem: TransactionData): boolean {

    if (!this.searchFilters.fromDate && !this.searchFilters.toDate) {
      return true;    // omit this filter
    } else if (this.searchFilters.fromDate && this.searchFilters.toDate) {
      return arrayItem.date.isBetween(this.searchFilters.fromDate, this.searchFilters.toDate);
    } else if (!this.searchFilters.toDate) {
      return arrayItem.date.isAfter(this.searchFilters.fromDate);
    } else if (!this.searchFilters.fromDate) {
      return arrayItem.date.isBefore(this.searchFilters.toDate);
    }
  }

  private verifyByAccount(arrayItem: TransactionData): boolean {
    if (!this.searchFilters.accountType) {
      return true;
    }

    return arrayItem.accountType === this.searchFilters.accountType;
  }

  private verifyByMessage(arrayItem: TransactionData): boolean {

    if (!this.searchFilters.message) {
      return true;
    }
    return _.some(arrayItem.transactionMessage,
      (item) => {
        return _.includes(arrayItem.transactionMessage.toLowerCase(), this.searchFilters.message.toLowerCase());
      });
  }

  private verifyByStatus(arrayItem: TransactionData): boolean {
    if (!this.searchFilters.statusType) {
      return true;  // If there is no statusType filter, that way we omit this verification
    }

    return arrayItem.statusType === this.searchFilters.statusType;
  }

  filterByAccountAndStatus(): Array<TransactionData> {
    const filtered: Array<TransactionData> = _.filter(this.dataArray,
      item => this.verifyByAccount(item) &&
        this.verifyByStatus(item) && this.verifyByDate(item) && this.verifyByMessage(item));

    return filtered;
  }


}

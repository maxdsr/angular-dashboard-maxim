import * as moment from 'moment';
import {Moment} from 'moment';
import {SearchTransactionDataModel} from './search-transaction-data-model';
import {SearchFilters} from '../form-component/form-component.component';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

export enum Accounts {
  first = 'first',
  third = 'third',
  needed = 'needed',
  credit = 'credit',
  bitcoin = 'bitcoin',
  any = 'Orice tip de plată'
}

export enum StatusType {
  success = 'Procesată cu succes',
  error = 'Procesată cu eroare',
  any = 'Orice tip de statut'
}

export interface TransactionData {
  statusType: StatusType;
  date: Moment;
  cardHolderName: string;
  iban: string;
  transactionMessage: string;
  amount: number;
  accountType: Accounts;
}

@Injectable()
export class TransactionDataService {

  private dataArray: Array<TransactionData> = [];
  private shuffledDataArray: Array<TransactionData>;
  private readonly MOCK_DATA_URL: string = './assets/mocks/data.json';
  private _httpRequestWasNotMade = true;

  constructor(private httpClient: HttpClient) {

  }


  private setUpDate(inputDate?: string): Moment {

    if (inputDate) {
      return moment(inputDate, 'DD-MM-YYYY').locale('ro');
    } else {
      return moment().locale('ro');
    }
  }

  getHttpRequest(): Observable<Object> {
    return this.httpClient.get(this.MOCK_DATA_URL);
  }

  insertDataToArray(data: any[]) {
    for (let i = 0; i < data.length; i++) {
      data[i]['date'] = this.setUpDate(data[i]['date']);    // Replacing string date with MomentJS date
      this.dataArray.push(data[i]);
    }

    this.shuffleDataArray();
  }

  startSearchByFilters(data: SearchFilters): Array<TransactionData> {
    const pac = new SearchTransactionDataModel(data, this);
    return pac.filterByAccountAndStatus();
  }

  shuffleDataArray() {

    this.shuffledDataArray = this.dataArray
      .map((a) => ({sort: Math.random(), value: a}))
      .sort((a, b) => a.sort - b.sort)
      .map((a) => a.value);
  }

  getShuffledArray(): Array<TransactionData> {
    return this.shuffledDataArray;
  }

  httpRequestWasNotMade(): boolean {
    return this._httpRequestWasNotMade;
  }

  httpRequestWasMade() {
    this._httpRequestWasNotMade = false;
  }
}


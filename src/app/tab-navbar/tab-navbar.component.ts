import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-tab-navbar',
  templateUrl: './tab-navbar.component.html',
  styleUrls: ['./tab-navbar.component.less']
})
export class TabNavbarComponent implements OnInit {

  protected readonly messagesNumberObject: any;

  constructor() {

    this.messagesNumberObject = {
        archive: 0,
        authorization: 119,
        pending: 3,
        errors: 0
      };
  }

  protected displayMessagesNumber(msgNr: number): string {
    if (msgNr === 0) {
      return;
    }
    return '(' + msgNr.toString() + ')';
  }

  ngOnInit() {
  }

}
